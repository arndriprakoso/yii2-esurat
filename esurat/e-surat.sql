-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 10, 2019 at 07:36 PM
-- Server version: 10.1.29-MariaDB-6
-- PHP Version: 7.0.29-1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `e-surat`
--

-- --------------------------------------------------------

--
-- Table structure for table `appoval_rules_node`
--

CREATE TABLE `appoval_rules_node` (
  `id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  `bisa_menandatangani` tinyint(1) DEFAULT NULL,
  `bisa_atas_nama` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `approval_rules_edge`
--

CREATE TABLE `approval_rules_edge` (
  `id` int(11) NOT NULL,
  `parent_rules_node_id` int(11) NOT NULL,
  `child_rules_node_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `approval_surat_keluar`
--

CREATE TABLE `approval_surat_keluar` (
  `id` int(11) NOT NULL,
  `surat_keluar_id` int(11) NOT NULL,
  `jabatan_users_id` int(11) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` text,
  `dari_jabatan_users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `disposisi_rules_edge`
--

CREATE TABLE `disposisi_rules_edge` (
  `id` int(11) NOT NULL,
  `parent_node_id` int(11) NOT NULL,
  `child_node_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `disposisi_rules_node`
--

CREATE TABLE `disposisi_rules_node` (
  `id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  `penerima_surat` tinyint(1) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `disposisi_surat_masuk`
--

CREATE TABLE `disposisi_surat_masuk` (
  `id` int(11) NOT NULL,
  `surat_masuk_id` int(11) NOT NULL,
  `jabatan_users_id` int(11) NOT NULL,
  `status` varchar(20) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `keterangan` varchar(45) DEFAULT NULL,
  `dari_jabatan_users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `golongan`
--

CREATE TABLE `golongan` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `instansi`
--

CREATE TABLE `instansi` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `alamat` text,
  `no_telepon` varchar(20) DEFAULT NULL,
  `kode_pos` varchar(10) DEFAULT NULL,
  `fax` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `instansi`
--

INSERT INTO `instansi` (`id`, `nama`, `alamat`, `no_telepon`, `kode_pos`, `fax`) VALUES
(1, 'PT. Eklanku Indonesia Cemerlang', 'Lamongan', '085736457209', '62291', ''),
(2, 'PT. Indomarco', 'Lamongan', '08578982631', '69822', '998075'),
(3, 'PT. Djarum', 'Lamongan', '08578982789', '69223', ''),
(4, 'Bank Jatim', 'Lamongan', '08578981234', '69732', ''),
(5, 'Bank Central Asia', 'Lamongan', '03223101388', '62291', NULL),
(6, 'Bank Rakyat Indonesia', 'Lamongan', '085739019283', '62678', NULL),
(7, 'Bank Nasional Indonesia', 'Lamongan', '085790452819', '62299', NULL),
(8, 'Bank Bukopin', 'Lamongan', '087756783241', '62291', NULL),
(9, 'Bank Cimb Niaga', 'Lamongan', '057982436182', '62391', NULL),
(10, 'PT. Gudang Garam', 'Lamongan', '085798098767', '62332', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `jabatan`
--

CREATE TABLE `jabatan` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL,
  `instansi_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `jabatan_users`
--

CREATE TABLE `jabatan_users` (
  `id` int(11) NOT NULL,
  `jabatan_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  `golongan_id` int(11) NOT NULL,
  `instansi_id` int(11) NOT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_surat`
--

CREATE TABLE `kategori_surat` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `kategori_surat_keluar`
--

CREATE TABLE `kategori_surat_keluar` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `sifat`
--

CREATE TABLE `sifat` (
  `id` int(11) NOT NULL,
  `nama` varchar(45) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `surat_keluar`
--

CREATE TABLE `surat_keluar` (
  `id` int(11) NOT NULL,
  `nomor_klasifikasi` varchar(45) DEFAULT NULL,
  `file_lampiran` varchar(45) DEFAULT NULL,
  `perihal` text,
  `jabatan_id` int(11) NOT NULL,
  `sifat_id` int(11) NOT NULL,
  `kategori_surat_id` int(11) NOT NULL,
  `isi_surat` text,
  `isi_lampiran_surat` text,
  `no_agenda` varchar(45) DEFAULT NULL,
  `tanggal` datetime DEFAULT NULL,
  `instansi_id` int(11) NOT NULL,
  `approval_surat_keluar_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `surat_masuk`
--

CREATE TABLE `surat_masuk` (
  `id` int(11) NOT NULL,
  `nomor_agenda` varchar(45) DEFAULT NULL,
  `no_surat` varchar(45) DEFAULT NULL,
  `surat_dari` varchar(45) DEFAULT NULL,
  `instansi_id` int(11) NOT NULL,
  `is_antar_dinas` tinyint(1) DEFAULT NULL,
  `kategori_surat_id` int(11) NOT NULL,
  `sifat_id` int(11) NOT NULL,
  `no_tindak_lanjut` varchar(45) DEFAULT NULL,
  `perihal` text,
  `tanggal` date DEFAULT NULL,
  `lampiran` varchar(45) DEFAULT NULL,
  `file_surat` varchar(45) DEFAULT NULL,
  `file_lampiran` varchar(45) DEFAULT NULL,
  `jumlah_lampiran` int(11) DEFAULT NULL,
  `jabatan_users_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(45) NOT NULL,
  `auth_key` varchar(255) NOT NULL,
  `password_hash` varchar(255) DEFAULT NULL,
  `password_reset_token` varchar(255) DEFAULT NULL,
  `email` varchar(45) NOT NULL,
  `status` varchar(45) DEFAULT NULL,
  `role` varchar(45) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appoval_rules_node`
--
ALTER TABLE `appoval_rules_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_appoval_rules_node_instansi1_idx` (`instansi_id`),
  ADD KEY `fk_appoval_rules_node_jabatan1_idx` (`jabatan_id`);

--
-- Indexes for table `approval_rules_edge`
--
ALTER TABLE `approval_rules_edge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_approval_rules_edge_appoval_rules_node1_idx` (`parent_rules_node_id`),
  ADD KEY `fk_approval_rules_edge_appoval_rules_node2_idx` (`child_rules_node_id`);

--
-- Indexes for table `approval_surat_keluar`
--
ALTER TABLE `approval_surat_keluar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_approval_surat_keluar_surat_keluar1_idx` (`surat_keluar_id`),
  ADD KEY `fk_approval_surat_keluar_jabatan_users1_idx` (`jabatan_users_id`),
  ADD KEY `fk_approval_surat_keluar_jabatan_users2_idx` (`dari_jabatan_users_id`);

--
-- Indexes for table `disposisi_rules_edge`
--
ALTER TABLE `disposisi_rules_edge`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_surat_rules_edge_surat_rules_node1_idx` (`parent_node_id`),
  ADD KEY `fk_surat_rules_edge_surat_rules_node2_idx` (`child_node_id`);

--
-- Indexes for table `disposisi_rules_node`
--
ALTER TABLE `disposisi_rules_node`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_surat_rules_node_instansi1_idx` (`instansi_id`),
  ADD KEY `fk_surat_rules_node_jabatan1_idx` (`jabatan_id`);

--
-- Indexes for table `disposisi_surat_masuk`
--
ALTER TABLE `disposisi_surat_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_disposisi_surat_masuk_surat_masuk1_idx` (`surat_masuk_id`),
  ADD KEY `fk_disposisi_surat_masuk_jabatan_users1_idx` (`jabatan_users_id`),
  ADD KEY `fk_disposisi_surat_masuk_jabatan_users2_idx` (`dari_jabatan_users_id`);

--
-- Indexes for table `golongan`
--
ALTER TABLE `golongan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `instansi`
--
ALTER TABLE `instansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jabatan_instansi1_idx` (`instansi_id`);

--
-- Indexes for table `jabatan_users`
--
ALTER TABLE `jabatan_users`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_jabatan_users_jabatan1_idx` (`jabatan_id`),
  ADD KEY `fk_jabatan_users_users1_idx` (`users_id`),
  ADD KEY `fk_jabatan_users_golongan1_idx` (`golongan_id`),
  ADD KEY `fk_jabatan_users_instansi1_idx` (`instansi_id`);

--
-- Indexes for table `kategori_surat`
--
ALTER TABLE `kategori_surat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kategori_surat_keluar`
--
ALTER TABLE `kategori_surat_keluar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sifat`
--
ALTER TABLE `sifat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_surat_keluar_jabatan1_idx` (`jabatan_id`),
  ADD KEY `fk_surat_keluar_sifat1_idx` (`sifat_id`),
  ADD KEY `fk_surat_keluar_kategori_surat1_idx` (`kategori_surat_id`),
  ADD KEY `fk_surat_keluar_instansi1_idx` (`instansi_id`),
  ADD KEY `fk_surat_keluar_approval_surat_keluar1_idx` (`approval_surat_keluar_id`);

--
-- Indexes for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_surat_masuk_instansi_idx` (`instansi_id`),
  ADD KEY `fk_surat_masuk_kategori_surat_masuk1_idx` (`kategori_surat_id`),
  ADD KEY `fk_surat_masuk_sifat1_idx` (`sifat_id`),
  ADD KEY `fk_surat_masuk_jabatan_users1_idx` (`jabatan_users_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `appoval_rules_node`
--
ALTER TABLE `appoval_rules_node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `approval_rules_edge`
--
ALTER TABLE `approval_rules_edge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `approval_surat_keluar`
--
ALTER TABLE `approval_surat_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disposisi_rules_edge`
--
ALTER TABLE `disposisi_rules_edge`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disposisi_rules_node`
--
ALTER TABLE `disposisi_rules_node`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `disposisi_surat_masuk`
--
ALTER TABLE `disposisi_surat_masuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `golongan`
--
ALTER TABLE `golongan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `instansi`
--
ALTER TABLE `instansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `jabatan`
--
ALTER TABLE `jabatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `jabatan_users`
--
ALTER TABLE `jabatan_users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sifat`
--
ALTER TABLE `sifat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `appoval_rules_node`
--
ALTER TABLE `appoval_rules_node`
  ADD CONSTRAINT `fk_appoval_rules_node_instansi1` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_appoval_rules_node_jabatan1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `approval_rules_edge`
--
ALTER TABLE `approval_rules_edge`
  ADD CONSTRAINT `fk_approval_rules_edge_appoval_rules_node1` FOREIGN KEY (`parent_rules_node_id`) REFERENCES `appoval_rules_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_approval_rules_edge_appoval_rules_node2` FOREIGN KEY (`child_rules_node_id`) REFERENCES `appoval_rules_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `approval_surat_keluar`
--
ALTER TABLE `approval_surat_keluar`
  ADD CONSTRAINT `fk_approval_surat_keluar_jabatan_users1` FOREIGN KEY (`jabatan_users_id`) REFERENCES `jabatan_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_approval_surat_keluar_jabatan_users2` FOREIGN KEY (`dari_jabatan_users_id`) REFERENCES `jabatan_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_approval_surat_keluar_surat_keluar1` FOREIGN KEY (`surat_keluar_id`) REFERENCES `surat_keluar` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disposisi_rules_edge`
--
ALTER TABLE `disposisi_rules_edge`
  ADD CONSTRAINT `fk_surat_rules_edge_surat_rules_node1` FOREIGN KEY (`parent_node_id`) REFERENCES `disposisi_rules_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_rules_edge_surat_rules_node2` FOREIGN KEY (`child_node_id`) REFERENCES `disposisi_rules_node` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disposisi_rules_node`
--
ALTER TABLE `disposisi_rules_node`
  ADD CONSTRAINT `fk_surat_rules_node_instansi1` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_rules_node_jabatan1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `disposisi_surat_masuk`
--
ALTER TABLE `disposisi_surat_masuk`
  ADD CONSTRAINT `fk_disposisi_surat_masuk_jabatan_users1` FOREIGN KEY (`jabatan_users_id`) REFERENCES `jabatan_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disposisi_surat_masuk_jabatan_users2` FOREIGN KEY (`dari_jabatan_users_id`) REFERENCES `jabatan_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_disposisi_surat_masuk_surat_masuk1` FOREIGN KEY (`surat_masuk_id`) REFERENCES `surat_masuk` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jabatan`
--
ALTER TABLE `jabatan`
  ADD CONSTRAINT `fk_jabatan_instansi1` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `jabatan_users`
--
ALTER TABLE `jabatan_users`
  ADD CONSTRAINT `fk_jabatan_users_golongan1` FOREIGN KEY (`golongan_id`) REFERENCES `golongan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jabatan_users_instansi1` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jabatan_users_jabatan1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_jabatan_users_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `surat_keluar`
--
ALTER TABLE `surat_keluar`
  ADD CONSTRAINT `fk_surat_keluar_instansi1` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_keluar_jabatan1` FOREIGN KEY (`jabatan_id`) REFERENCES `jabatan` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_keluar_kategori_surat1` FOREIGN KEY (`kategori_surat_id`) REFERENCES `kategori_surat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_keluar_sifat1` FOREIGN KEY (`sifat_id`) REFERENCES `sifat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `surat_masuk`
--
ALTER TABLE `surat_masuk`
  ADD CONSTRAINT `fk_surat_masuk_instansi` FOREIGN KEY (`instansi_id`) REFERENCES `instansi` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_masuk_jabatan_users1` FOREIGN KEY (`jabatan_users_id`) REFERENCES `jabatan_users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_masuk_kategori_surat_masuk1` FOREIGN KEY (`kategori_surat_id`) REFERENCES `kategori_surat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `fk_surat_masuk_sifat1` FOREIGN KEY (`sifat_id`) REFERENCES `sifat` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
